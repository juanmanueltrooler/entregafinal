import './App.css';
import Logo from './logo.svg';
import imagen from './mico.jpg'
import Input from './components/input';
import TextArea from './components/textarea';
import Button from './components/button';
import Paragrah from './components/paragrahp';
  const Aux = () => {
    return(
      <>
        <main className='curriculum'>
        <div className='curriculum__name'>
          <div className='curriculum__name__img'>
            <img src={imagen} className="imgagen" />
          </div>
          <div className='content'>
            <Paragrah
              className="curriculum__name__content__p-1__p-1"
              texto="Juan Manuel Restrepo Osorio"
            />
            <Paragrah
              className="curriculum__name__content__p-1__p-2"
              texto="22 años, Bachiller Técnico"
            />
          </div>
        </div>

        <div className='curriculum__info'>
          <Paragrah
            className="curriculum__info__titulo"
            texto="Informacíon personal"
          />
          <Paragrah
            className="curriculum__info__datos"
            texto="Nombre y apellido :"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="Juan Manuel Restrepo Osorio"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="Juan Manuel Restrepo Osorio"
          />
          <br></br>
          <Paragrah
            className="curriculum__info__datos"
            texto="Email :"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="jmrestrepo93610@umanizales.edu.co"
          />
          <br></br>
          <Paragrah
            className="curriculum__info__datos"
            texto="Direccíon :"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="Cra:49 # 103-89"
          />
          <br></br>
          <Paragrah
            className="curriculum__info__datos"
            texto="Telefono :"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="716252829"
          />
          <br></br>
          <Paragrah
            className="curriculum__info__datos"
            texto="Cedula :"
          />
          <Paragrah
            className="curriculum__info__infodato"
            texto="1008227356"
          />
        </div>
      
        <main className='formulario'>
          <img src={Logo} className="App-logo" alt="logo" />
          <h1 className='formulario__title'>Contacto</h1>
          <div className='formulario__contenedor'>
            <br></br>
            <Input
              type="text"
              label="Nombre"
              name="Nombre"
              placeholder="Nombre Completo"
            />
            <Input
              type="text"
              label="Email"
              name="Email"
              placeholder="Email"
            />
            <Input
              type="text"
              label="Asunto"
              name="Asunto"
              placeholder="Asunto"
            />
            <Input
              type="number"
              label="Telefono"
              name="Asunto"
              placeholder="telefono"
            />
            <TextArea
              label="Mensaje"
              name="Mensaje"
            />
            <Button
              type="button"
              className='Formulario__btn btn btn-success'
              mensaje='Enviar'
            />
          </div>
        </main>
        </main>
      </>
    );
};


export default Aux;
