import React from "react";

const ComponentInput = ({label, type ,name ,placeholder}) =>{
    return(
        <div>
            <label className='Formulario__label'>{label}
            <input 
                type={type}
                name={name}
                placeholder={placeholder}
                className='Formulario__input'
            />
            </label>
        </div>
    );
}

export default ComponentInput;