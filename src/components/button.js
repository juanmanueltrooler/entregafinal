import React from "react";

const ComponentButton = ({type , className , mensaje}) =>{
    return(
        <button
            type={type} 
            className={className}
        >
            {mensaje}
        </button>
    );
}

export default ComponentButton;