import React from "react";

const ComponentParagrah = ({className, texto}) =>{
    return(
        <p className={className}>{texto}</p>
    );
}

export default ComponentParagrah;