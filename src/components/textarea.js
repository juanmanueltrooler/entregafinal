import React from "react";

const ComponentTeaxtarea = ({label,name}) =>{
    return(
        <div>
            <label className='Formulario__label'>{label}</label>
            <div class="form-floating Formulario__label">
            <textarea class="form-control Formulario__textarea"
                rows="5"
                cols="50"
                name={name} ></textarea>
            </div>
        </div>
    );
}

export default ComponentTeaxtarea;